# Autor
Julian Ontibon

# Resumen del proyecto

Utilizando el aplicativo libre blender se modelara un vehiculo que tenga
caracteristicas low poly, ademas se realizara un dibujo guia en krita.

# Justificaciòn

Cuando se habla de productos audiovisuales en 3D siempre se mencionan
programas como 3ds Max o Maya, los cuales son privativos de la empresa
Autodesk, mi idea con este proyecto es mostrar que con sofware libre se
pueden realizar el mismo tipo de modelado, teniendo el beneficio de que
blender es un programa libre y que ademas consume menos recursos en
terminos de desempeño de la maquina.

# Alcance y limitaciones

Se realizara un modelado basico, con la poca tecnica que manejo sobre
el modelado 3D, con base a un dibujo guia se tratara de hacer la
geometria low poly.

# Objetivos

Renderizar el modelado del vehiculo con blender.
Dibujar la el carro guia en krita.
Aplicar texturas e iluminacion de la geometria dentro de blender.

# Beneficiarios

Toda persona que este interesada en aprender modelado 3D junto con
animaciones se puede motivar a implementarlo en sofware libre si visualiza
este proyecto.

# Links referencia

https://free3d.com/es/modelo-3d/911-gt2-1995-rimwith-a048-tyre-brake-disc-
and-caliper-32559.html

Modelado de las ruedas.

https://free3d.com/es/modelo-3d/911-gt2-1995-rimwith-a048-tyre-brake-disc-
and-caliper-32559.html

Fondo 360.

Los dos enlaces mencionados son de carácter libre.
